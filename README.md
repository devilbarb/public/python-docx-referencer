## Python Docx reference number replacer

Finds reference number in source document via regex, looks up reference text from second document, inputs reference code, new code and reference into a lookup document and replaces the reference code with new code in original document

## Getting Started

edit replace.py with file names  
python3 replace.py

### Prerequisites

pip3 install python-docx docx
