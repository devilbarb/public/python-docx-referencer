#!/usr/bin/env python3
#
# Author: James Barber
# Desc: Replaces "filename" document regex syntax with code in refname 
#

import re
from docx import Document
from docx.text.run import Run

filename = "test.docx"
refname = "references.docx"
newdocname = "reference-list.docx"

doc = Document(filename)
refdoc = Document(refname)
newdoc = Document()

def remove_duplicates(duplicate):
    final_list = []
    for num in duplicate:
        if num not in final_list:
            final_list.append(num)
    return final_list

def find_ref_text(doc, reference_number):

    tablenum = 0
    runs = []
    for table in doc.tables:

        rownum = 0
        rows = table.rows
        for row in table.rows:
            cellnum = 0
            rownum = rownum + 1
            for cell in row.cells:
                cellnum = cellnum + 1
                if cell.text.find(reference_number) != -1:
                    print("tablenum: " + str(tablenum) + " | rownum: " + str(rownum) + " | cellnum: " + str(cellnum))
                    refparagraph = row.cells[2].paragraphs[0]
                    return refparagraph

        tablenum = tablenum + 1

def create_reference_row(refmatch, refnumber, refparagraph):
    tmprefnumber = str(refnumber + 1)
    reftable.add_row()
    reftable.cell(refrows, 0).text = str(refmatch)
    reftable.cell(refrows, 1).text = str(tmprefnumber + ".")
    refcell = reftable.cell(refrows, 2).paragraphs[0]

    for i in range(len(refparagraph.runs)):
        refcell.add_run(refparagraph.runs[i].text)
        refcell.runs[i].bold = refparagraph.runs[i].bold
        refcell.runs[i].italic = refparagraph.runs[i].italic
        refcell.runs[i].underline = refparagraph.runs[i].underline
        refcell.runs[i].style = refparagraph.runs[i].style

def docx_replace_regex(doc_obj, regex , replace, reset):
    print(regex)
    for p in doc_obj.paragraphs:

        if regex.search(p.text):
            print("Matched paragraph")
            inline = p.runs
            # Loop added to work with runs (strings with same style)
            for i in range(len(inline)):
                # Basic merge check as Word f's up strings across multiple runs
                regex_found = regex.search(inline[i].text)
                if regex_found is None:

                    if i < len(inline) - 1:
                        j = i + 1
                        future_find = regex.search(inline[j].text)

                        if future_find is None:
                            print("Running combined runs check")
                            combined_check = inline[i].text + inline[j].text
                            regex_found = regex.search(combined_check)

                            if regex_found is not None:
                                print("Multiline run match: " + str(i) + " and " + str(j))
                                inline[i].text = inline[i].text+inline[j].text
                                print("J previous text: " + inline[j].text)
                                inline[j].text = ''

                    elif i == len(inline):
                        print("Could not match reference number within run")
                        exit(5)

                if regex_found is not None:
                    print("Matched run")

                    # Remove ref number from document, add new run before with new superscript reference number
                    if reset == None:
                        text_dictionary = split_run(inline[i], regex, replace)
                        text_list = text_dictionary["list"]
                        list_length = len(text_list)
                        print("\n".join(text_list))

                        if debug == True:
                            print(text_dictionary)

                        if(list_length == 1):
                            inline[i].text = text_list[0]
                            inline[i].font.superscript = True

                        else:
                            for j in range(list_length):
                                #Create new run
                                new_run_element = p._element._new_r()
                                if j == 0:
                                    inline[i]._element.addprevious(new_run_element)
                                if j == 1:
                                    inline[i].text = text_list[j]
                                    if j == text_dictionary['location']:
                                        inline[i].font.superscript = True
                                    else:
                                        inline[i].font.superscript = False

                                if j == 2:
                                    inline[i]._element.addnext(new_run_element)
                                if j != 1:
                                    new_run = Run(new_run_element, inline[i]._parent)
                                    new_run.text = text_list[j]
                                    new_run.italic = inline[i].italic
                                    if j == text_dictionary['location']:
                                        new_run.font.superscript = True
                                        new_run.italic = False
                    else:
                        inline[i].text = regex.sub('', inline[i].text)
                        return

    for table in doc_obj.tables:
        for row in table.rows:
            for cell in row.cells:
                docx_replace_regex(cell, regex, replace, reset)

def split_run(run, regex, replace):
    max_length = len(run.text)
    print(run.text)
    string_location = regex.search(run.text).span()
    string_size = string_location[1] - string_location[0]
    if debug == True:
        print("string_location: " + str(string_location) + " string size: " + str(string_size) + " run text size: " + str(max_length))

    text_list = []
    text_dictionary = dict()
    code_location = 0


    if string_location[0] == 0 & string_location[1] == max_length:
        text_list.append("[" + replace + "]")
    else:

        if string_location[0] == 0:
            text_list.append("[" + replace + "]")

        if string_location[0] == 0 & string_location[1] != max_length:
            text_list.append(regex.sub('', run.text[string_location[1]:]))

        if string_location[0] > 0:
            code_location = 1
            text_list.append(regex.sub('', run.text[0:string_location[0]]))

            if string_location[1] < max_length:
                text_list.append("[" + replace + "]")
                text_list.append(regex.sub('', run.text[string_location[1]:]))
            else:
                text_list.append("[" + replace + "]")

    text_dictionary["list"] =  text_list
    text_dictionary["location"] = code_location
    return text_dictionary

# Create table within new doc
debug = False
refrows = 0
refnumber = 0

reftable = newdoc.add_table(rows=0, cols=3)
regex = re.compile(r'#00\/([a-zA-Z0-9]{5})')
#regex = re.compile(r"#00\/(N0080)")


for para in doc.paragraphs:
    # Cycle paragraphs matching all regex in a paragraph
    findall = re.findall(regex, para.text)
    findall = remove_duplicates(findall)

    if debug == True:
        print(findall)

    if len(findall) > 0:
        for refmatch in findall:
            if refrows == -1:
                break

            print(refmatch)
            replace_regex = re.compile(r"#00\/" + refmatch)

            if refmatch == "reset":
                refnumber = 0
                print("Resetting reference number")
                docx_replace_regex(doc, replace_regex, '', 1)
                continue

            # Obtain reference number, look up in reference doc
            print("Finding ref text: " + refmatch)
            refparagraph = find_ref_text(refdoc, refmatch)
            if refparagraph == None:
                print("Reference number could not be found in reference document: " + refmatch)
                break
            # Add row to table, populate with ref number, new index and ref quote
            print("Creating ref row: " + refmatch)
            create_reference_row(refmatch, refnumber, refparagraph)

            # Replace regex in original document with new subscript index
            print("Replacing: " + refmatch + " with " + str(refnumber + 1))
            docx_replace_regex(doc, replace_regex, str(refnumber + 1), None)
            refnumber = refnumber + 1
            refrows = refrows + 1

doc.save('replaced.docx')
newdoc.save(newdocname)
